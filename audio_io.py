#!/usr/bin/python3
"""
Module containing basic IO functions to be used for audio and Text to Speech
"""
import io, subprocess, requests, os
from stat import S_IRWXU, S_IRWXG, S_IRWXO
from tts_client import StaticTTS

def write_binary(path, audio_bin):
    """
    Writes the given bytes to the given path     
    Arguments:
        path {[str]} -- [path to write to]
        audio_bin {[bytes]} -- [Audio to be written]
    """
    with io.open(path, "wb") as file:
        file.write(audio_bin)
        # same as chmod 777 path to ensure abilty to overwrite
        os.chmod(path, S_IRWXU | S_IRWXG | S_IRWXO)

def play(path):
    """
    Plays the given file through aplay, then removes it.
    Note: file should be readable by aplay.
    Arguments:
        path {[str]} -- [path to the file being played]
    """
    subprocess.run(["bash", "/home/pi/razhome/razhome-speech-daemon/play.sh", path])

def play_mult(path):
    """Plays an audio file"""
    subprocess.run(["bash", "/home/pi/razhome/razhome-speech-daemon/playmult.sh", path])

def say_tts(text):
    """
    Uses Google Cloud Speech api
    
    Arguments:
        text {[type]} -- [description]
    """
    StaticTTS.client.say(text)
    print("Said: {}".format(text))


import os, sys

if not 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ:
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/pi/razhome/creds/iam-key.json'
    print("WARNING: GOOGLE_APPLICATION_CREDENTIALS not set. Default value supplied", file=sys.stderr)
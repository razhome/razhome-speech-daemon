#!/usr/bin/python3
"""
Runs a basic http server on local host which contains endpoints for Text-to-Speech
services through Google Cloud.
"""

import check_env
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib import parse
import audio_io, signal, sys
from stt_client import STTClient

server = None

class MainHandler(BaseHTTPRequestHandler):
    """
    Extends BaseHTTPRequestHandler for use in a Text to Speech/Voice command server
    """
    def do_GET(self):
        """
        Called when the server recieves a get command. Responds to the following API paths:
        /say?text=[text]            -   Outputs [text] to the speakers via Google Cloud TTS
        /play?file=[path]           -   Attempts to play the audio file located at [path]
        """
        parsed_path = parse.urlparse(self.path)
        parsed_query = parse.parse_qs(parsed_path.query)
        
        # API Bindings
        # Say something with TTS
        if(parsed_path.path == "/say"):
            print("Recived GET on /say")
            if(parsed_query['text']):
                audio_io.say_tts(parsed_query['text'][0])
            else:
                print("Invalid query")
                self.send_error(400, "Invalid Query")
                self.send_header('Content-Type', 'text/plain; charset=utf-8')
                self.end_headers()
                return
        # Play an audio file
        elif(parsed_path.path == "/play"):
            print("Recieved GET on /play")
            if(parsed_query['file']):
                audio_io.play(parsed_query['file'][0])
            else:
                print("Invalid query")
                self.send_error(400, "Invalid Query")
                self.send_header('Content-Type', 'text/plain; charset=utf-8')
                self.end_headers()
                return
        # Try to run a razhome command        
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain; charset=utf-8')
        self.end_headers()

# Starts command listener
stt = STTClient()
stt.start_detection()

# Hosts to localhost:3000
server = HTTPServer(('localhost', 3000), MainHandler)
print('Starting server, use <Ctrl-C> to stop')
server.serve_forever()

def main():
    try:
        # Starts command listener
        stt = STTClient()
        stt.start_detection()
        
        # Hosts to localhost:3000
        server = HTTPServer(('localhost', 3000), MainHandler)
        print('Starting server, use <Ctrl-C> to stop')
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()
        stt.stop_detection()
    except SystemExit:
        server.shutdown()
        stt.stop_detection()

if(__name__=="__main__"):
    main()
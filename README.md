# RazHome Speech Daemon

Implements the google cloud TTS and STT APIs to provide the interactive front end for the RazHome device

## Server details
By default, the server is hosted on Port 3000

## Installation
Running `deps.sh` will read `deps.json` and install all dependancies through `apt` and `pip3`
It assumes you already have `python3` and `python3-pip` installed.

The `systemd` unit file is available in the [RazHome Scripts](https://gitlab.com/razhome/razhome-scripts) repository.
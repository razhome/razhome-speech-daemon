"""
Simple helper module to use snowboy to detect a hotword and then return
"""
from snowboy import snowboydecoder
import speech_recognition as sr

detected = False

def _detected_cb():
    global detected
    detected = True

def _interrupt_cb():
    global detected
    return detected

class KeywordDetector(object):
    def __init__(self, model, sensitivity=0.60, sleep_time=0.03):
        self.detector = snowboydecoder.HotwordDetector(model, sensitivity=sensitivity)
        self.sleep_time = sleep_time

    def await_keyword(self):
        global detected
        detected = False
        self.detector.start(detected_callback=_detected_cb, sleep_time=self.sleep_time, interrupt_check=_interrupt_cb)
        self.detector.terminate()

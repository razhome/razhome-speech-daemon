#!/usr/bin/python3
"""
Handlers for various voice command functions
"""
import json, pathlib
from audio_io import say_tts;
import subprocess
import requests

class CommandException(Exception):
    """
    Raised when an error occurs trying to execute a command.    
    """
    pass

def run_command(command, namespace):
    command_script = "/home/pi/razhome/packages/{0}/commands/{1}.sh".format(namespace, command)
    if(pathlib.Path(command_script).exists()):
        subprocess.run(['bash', command_script])
    else:
        raise CommandException("File {} not found".format(command_script))

def try_phrase(phrase):
    phrase = phrase.strip().lower()
    print("Trying phrase {0} now".format(phrase))
    res = requests.get("http://localhost:3001/api/voice")
    phrases = res.json()
    for phr in phrases:
        if (phr['phrase'].lower() == phrase.lower()):
            print("Running command {0}.{1}".format(phr['command'], phr['namespace']))
            try:
                run_command(phr['command'], phr['namespace'])
            except CommandException as e:
                print(e)
                say_tts("Unknown Errors")
            finally:
                return
    say_tts("Command not found")


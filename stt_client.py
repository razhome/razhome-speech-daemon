#!/usr/bin/python3
"""
A module containing a Speech-to-Text client that implements the SpeechRecognizer library.
"""
import speech_recognition as sr
import os, io, sys
import audio_io
import threading
from snowboy_utils import KeywordDetector
import commands


google_creds = None

class CredError(Exception):
    """Raised when the program fails to load the google cloud credentials"""
    pass

def get_google_creds():
    global google_creds
    if(google_creds == None):
        cred_path = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')
        if(cred_path):
            try:
                with io.open(cred_path, "r") as cred_file:
                    google_creds = cred_file.read()
            except IOError:
                tb = sys.exc_info()[2]
                raise CredError("Failed to open {0}".format(cred_path)).with_traceback(tb)
        else:
            raise CredError("GOOGLE_APPLICATION_CREDENTIALS is not defined in the environment")

class STTClient:
    """
    A class which handles all Speech to Text operations for the razhome object
    """
    def __init__(self):
        """
        Initializes the Speech to text client by loading the snowboy model
        and calibrating the microphone
        """
        self.recognizer = sr.Recognizer()
        self.microphone = sr.Microphone()
        self.running = False
        self.keyword_detector = KeywordDetector("/home/pi/razhome/razhome-speech-daemon/Razhome.pmdl")

        # get google cloud credentials
        try:
            get_google_creds()
        except CredError as err:
            print("Failed to get Google Credentials")
            print(err)

        # calibrate microphone
        with self.microphone as source:
            self.recognizer.adjust_for_ambient_noise(source)
        
    def await_trigger(self):
        """
        Returns when the trigger-phrase has been detected
        TODO: get library location from path
        """
        if(sys.platform != "win32"):
            print("Awaiting hotword")
            self.keyword_detector.await_keyword()
            print("Recieved hotword")
        return

    def listen(self):
        """
        Listens for a phrase and processes with Google Cloud Speech        
        Returns:
            [str] -- [The phrase which was processed]
        """

        with self.microphone as source:
            print("Listening for phrase")
            audio = self.recognizer.listen(source)
        global google_creds
        assert google_creds != None
        
        try:
            transcribed = self.recognizer.recognize_google_cloud(audio, credentials_json=google_creds)
            print("Recognized phrase: {0}".format(transcribed))
            return transcribed
        except sr.UnknownValueError:
            print("Failed to recognize audio")
            audio_io.say_tts("Sorry, I didn't quite get that")
            # play a sound
            return self.listen()
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
            audio_io.say_tts("Unknown Error")
            return None
        
    def detection_async(self):
        """
        To be run as a thread.
        """
        self.running = True
        while(self.running):
            self.await_trigger()
            if(self.running): # extra check is needed here to ensure that the stop_detection function works as intended
                audio_io.play_mult("/home/pi/razhome/razhome-scripts/audio/trig.wav")
                phrase = self.listen()
                audio_io.play_mult("/home/pi/razhome/razhome-scripts/audio/exec.wav")
                if (phrase != None):
                    commands.try_phrase(phrase)

    def start_detection(self):
        detect_thread = threading.Thread(target=self.detection_async, args=(), daemon=True)
        detect_thread.start()

    def stop_detection(self):   
        self.running = False

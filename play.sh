#!/bin/bash
# Evidently, this is faster than parsing the audio file with python. Go figure.
# TODO: allow user to specify output device
file=$1

aplay -Dhw:0 $file
rm $file

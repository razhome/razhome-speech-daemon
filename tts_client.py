#!/usr/bin/python3
"""
A module containing TTSClient, which allows for Text-to-Speech using the google cloud api
"""

from google.cloud import texttospeech
import io, sys
import subprocess
import audio_io

class TTSClient:
    """
    Creates a new TTSClient which will allow you to input text and play or save audio
    Returns:
        TTSClient -- The new TTS Client you created
    """
    def __init__(self, vname='en-GB-Wavenet-A', lang_code="en_GB"):
        self.client = texttospeech.TextToSpeechClient()
        self.voice = texttospeech.types.VoiceSelectionParams(
            language_code=lang_code, name=vname)
        self.audio_config = texttospeech.types.AudioConfig(
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)
    
    def say(self, text):
        """
        Queries Google TTS with the given text and plays the audio file that is returned
        TODO: remove relative paths

        Arguments:
            text {[str]} -- The text to be spoken
        """
        synthesis_input = texttospeech.types.SynthesisInput(text=text)
        res = self.client.synthesize_speech(synthesis_input, self.voice, self.audio_config)
        # Writes file to the disk and plays with aplay.
        # This ends up reducing the weight on the system
        # By eliminating the need for PulseAudio or ffmpeg.
        # TODO: find something even better than this.
        audio_io.write_binary("/home/pi/razhome/razhome-speech-daemon/output.wav", res.audio_content)
        audio_io.play("/home/pi/razhome/razhome-speech-daemon/output.wav")

class StaticTTS:
    """
    Class which allows you to access a preconfigured TTSClient
    """
    
    client = TTSClient()
